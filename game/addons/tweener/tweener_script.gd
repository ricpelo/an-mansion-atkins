extends Tween

const DEFAULT_DURATION = 0.5

var _tween_callbacks = {}

func _ready():
	connect("tween_completed", self, "_on_tween_completed")


func _on_tween_completed(object, key):
	var callback_key = "%s-%s" % [object, key]
	if callback_key in _tween_callbacks.keys():
		var function = _tween_callbacks[callback_key][0]
		var parms = _tween_callbacks[callback_key][1]
		if parms != null:
			function.call_func(parms)
		else:
			function.call_func()
		_tween_callbacks.erase(callback_key)


func _add_tween_callback(object, key, callback, parms):
	var callback_key = "%s-:%s" % [object, key]
	_tween_callbacks[callback_key] = [callback, parms]


func tween_property(node, property, from, to, duration=DEFAULT_DURATION, callback=null, parms=null):
	if callback != null:
		_add_tween_callback(node, property, callback, parms)
	interpolate_property(
		node, property, 
	    from, to, duration, 
	    Tween.TRANS_LINEAR, Tween.EASE_IN)
	start()


func move_control(node, from, to, duration=DEFAULT_DURATION, callback=null, parms=null):
	tween_property(node, "rect_position", from, to, duration, callback, parms)


func change_color(node, from, to, duration=DEFAULT_DURATION, callback=null, parms=null, only_node=false):
	var property = "modulate"
	if only_node:
		property = "self_modulate"
	tween_property(node, property, from, to, duration, callback, parms)


func fade_in(node, duration=DEFAULT_DURATION, callback=null, parms=null, only_node=false):
	change_color(
		node,
		Color(1, 1, 1, 0),
		Color(1, 1, 1, 1),
		duration,
		callback,
		parms,
		only_node)


func fade_out(node, duration=DEFAULT_DURATION, callback=null, parms=null, only_node=false):
	change_color(
		node,
		Color(1, 1, 1, 1),
		Color(1, 1, 1, 0),
		duration,
		callback,
		parms,
		only_node)


func fade_to_black(node, duration=DEFAULT_DURATION, callback=null, parms=null, only_node=false):
	change_color(
		node,
		Color(1, 1, 1, 1),
		Color(0, 0, 0, 1),
		duration,
		callback,
		parms,
		only_node)


func fade_from_black(node, duration=DEFAULT_DURATION, callback=null, parms=null, only_node=false):
	change_color(
		node,
		Color(0, 0, 0, 1),
		Color(1, 1, 1, 1),
		duration,
		callback,
		parms,
		only_node)
