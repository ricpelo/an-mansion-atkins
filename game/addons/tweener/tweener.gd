tool

extends EditorPlugin

func _enter_tree():
    add_custom_type("Tweener", "Tween", preload("tweener_script.gd"), preload("icon_tween.svg"))

func _exit_tree():
    # Clean-up of the plugin goes here
    remove_custom_type("Tweener")

