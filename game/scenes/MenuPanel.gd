extends ColorRect

func _ready():
	# Audio bus settings
	var music_enabled = SettingsManager.get_setting("audio", "music")
	var sfx_enabled = SettingsManager.get_setting("audio", "sfx")
	AudioServer.set_bus_mute(SettingsManager.MUSIC_BUS, not music_enabled)
	AudioServer.set_bus_mute(SettingsManager.SFX_BUS, not sfx_enabled)
	$SoundOptions/MusicCheckBox.pressed = music_enabled
	$SoundOptions/SoundCheckBox.pressed = sfx_enabled


func _on_ResetStoryButton_pressed():
	get_parent().get_node("Modal").show()
	

func _on_WebButton_pressed():
	OS.shell_open('https://ancientnightmares.itch.io/mansion-atkins')


func _on_MusicCheckBox_toggled(button_pressed):
	AudioServer.set_bus_mute(SettingsManager.MUSIC_BUS, not button_pressed)
	SettingsManager.save_settings()


func _on_SoundCheckBox_toggled(button_pressed):
	AudioServer.set_bus_mute(SettingsManager.SFX_BUS, not button_pressed)
	SettingsManager.save_settings()


func _on_Button_pressed():
	get_parent().get_node("AboutPanel").show()
