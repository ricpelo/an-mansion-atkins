extends ColorRect

func _on_ButtonCancel_pressed():
	hide()

func _on_ButtonOk_pressed():
	get_parent().get_node("StoryViewer").reset_story()
	hide()