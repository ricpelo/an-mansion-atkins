extends TextureRect

var menu_status = false

func _ready():
	DisableAutoQuit.connect("quitting_game", self, "quit_game")
	DisableAutoQuit.connect("device_back_button_pressed", self, "_on_BackButton_pressed")
	# Continue story
	$StoryViewer.init(self)

func show_cover(animate=true):
	$Cover.show()
	if animate:
		$AnimationPlayer.queue("Cover flip")

func show_image_viewer(texture):
	$ImageViewer/ScrollContainer/CenterContainer/TextureRect.texture = texture
	$ImageViewer.show()

func hide_cover():
	$Cover.hide()
	$AnimationPlayer.clear_queue()

func quit_game():
	# Save game settings
	SettingsManager.save_settings()
	get_tree().quit()

func _on_MenuButton_pressed():
	menu_status = not menu_status
	if menu_status:
		$AnimationPlayer.play("Menu Up")
	else:
		$AnimationPlayer.play_backwards("Menu Up")

func _on_BackButton_pressed():
	$StoryViewer.step_back()

func _on_FontButton_pressed():
	$StoryViewer.change_font()
	SettingsManager.save_settings()
