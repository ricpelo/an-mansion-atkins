extends Control

var viewer = null
var _cursor_pos = null


func init(viewer_instance):
	viewer = viewer_instance


func _on_button_down():
	_cursor_pos = get_viewport().get_mouse_position()


func click_under_threshold():
	return (_cursor_pos - get_viewport().get_mouse_position()).length() < SettingsManager.CLICK_THRESHOLD


func _on_ResetStoryButton_pressed():
	if click_under_threshold():
		viewer.reset_story()
