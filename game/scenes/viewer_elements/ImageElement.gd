extends Control

var viewer = null
var media = null
var parms = []
var _cursor_pos = null


func init(viewer_instance, media_name, media_parms):
	media = media_name
	viewer = viewer_instance
	parms = media_parms
	var texture = load(SettingsManager.GBE_MEDIA_PATH + "/" + media)
	$Image.set_texture(texture)
	# Hide frame if not meant to be shown
	if "noframe" in parms:
		$Frame.hide()
#	var w = get_parent().get_size().x - 12
#	var h = (w * texture.get_size().y) / texture.get_size().x
#	$Image.set_custom_minimum_size(Vector2(w, h))

func _on_ImageElement_button_down():
	_cursor_pos = get_viewport().get_mouse_position()

func _on_ImageElement_pressed():
	if not "nozoom" in parms:
		if (_cursor_pos - get_viewport().get_mouse_position()).length() < SettingsManager.CLICK_THRESHOLD:
			viewer.main.show_image_viewer($Image.texture)
