extends Container

const HORIZONTAL_MARGIN = 15
const VERTICAL_MARGIN = 40

onready var portrait = get_node("PanelContainer/HBoxContainer/VBoxContainer/TextureRect")
onready var namelabel = get_node("PanelContainer/HBoxContainer/VBoxContainer/Label")
onready var textlabel = get_node("PanelContainer/HBoxContainer/RichTextLabel")

var viewer = null
var _first_drawn = true
var data = null

func init(viewer_instance, char_id, text):
	viewer = viewer_instance
	load_character(char_id, text)
	# Activate selectable text if in debug mode
	if SettingsManager.debug:
		textlabel.selection_enabled = true

func load_character(char_id, text):
	set_portrait(char_id)
	set_text(text)
	# Load character information from JSON
	data = Utils.load_json_file("%s/%s.character" % [SettingsManager.GBE_CHARACTER_DATA_PATH, char_id])
	if data != null:
		set_name(data["name"])
		namelabel.add_color_override("font_color", Color(data["color"]))
	else:
		set_name(char_id.capitalize())

func set_portrait(identifier):
	var texture = load("%s/%s.png" % [SettingsManager.GBE_CHARACTER_DATA_PATH, identifier])
	if texture != null:
		portrait.set_texture(texture)

func set_name(charname):
	namelabel.set_text(charname)

func set_text(text):
	textlabel.parse_bbcode(text)

func _on_RichTextLabel_draw():
	if _first_drawn:
		var w = textlabel.get_size().x - HORIZONTAL_MARGIN
		var h = textlabel.get_content_height() + VERTICAL_MARGIN
		textlabel.set_custom_minimum_size(Vector2(w, h))
		_first_drawn = false

func _on_RichTextLabel_meta_clicked(meta):
	pass
