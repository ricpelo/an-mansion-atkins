extends ScrollContainer

# Exports
export(PackedScene) var text_item_scene = null
export(PackedScene) var image_item_scene = null
export(PackedScene) var option_item_scene = null
export(PackedScene) var character_item_scene = null
export(PackedScene) var scene_item_scene = null
export(PackedScene) var story_end_item_scene = null
export(float) var background_change_time = 0.2
export(float) var section_change_time = 0.2
export(bool) var auto_scroll = false

export(Font) var small_font = null
export(Font) var medium_font = null
export(Font) var large_font = null

export(int) var DEFAULT_ITEM_SPACING = 20

# Main instance
var main = null
var tweener = null
var music_player = null
var sfx_player = null
var background = null
var fonts = []
var font_index = 0

var plugins_path = "res://scripts/gbe/plugins/"
var story_path = "res://story/"

# Status variables
onready var initial_size = rect_size
var _item_added = false
var _cleared = false
var _next_section = null
var current_options = []

func _ready():
	GBE.set_plugins_path(plugins_path)
	GBE.set_story_path(story_path)
	
	# Connect signals
	GBE.connect("processed_section", self, "_on_GBE_processed_section")
	GBE.connect("processed_text", self, "_on_GBE_processed_text")
	GBE.connect("processed_option", self, "_on_GBE_processed_option")
	GBE.connect("processed_jump", self, "_on_GBE_processed_jump")
	GBE.connect("processed_ending", self, "_on_GBE_processed_ending")
	GBE.connect("processed_conditional", self, "_on_GBE_processed_conditional")
	GBE.connect("processed_char_speech", self, "_on_GBE_processed_character_speech")
	GBE.connect("processed_styled_text", self, "_on_GBE_processed_styled_text")
	GBE.connect("processed_media", self, "_on_GBE_processed_media")
	GBE.connect("caught_error", self, "_on_GBE_caught_error")
	GBE.connect("processed_unknown_macro", self, "_on_GBE_processed_unknown_macro")
	GBE.connect("got_plugin_signal", self, "_on_GBE_got_plugin_signal")


func init(main_instance):
	main = main_instance
	music_player = main.get_node("MusicPlayer")
	sfx_player = main.get_node("SFXPlayer")
	tweener = main.get_node("Tweener")
	background = main.get_node("Background")
	
	font_index = SettingsManager.get_setting("display", "font_size")

	fonts = [
		small_font,
		medium_font,
		large_font
	]

	change_font(fonts[font_index], false)

	var state = null
	if not SettingsManager.debug:
		# Load state
		state = GBE.load_state(SettingsManager.GAME_STATE_PATH)
	if not state:
		GBE.reset_state()
		# Load module and start the adventure
		GBE.load_file(SettingsManager.GBE_INITIAL_MODULE)
		# Show cover and animate it
		if not SettingsManager.debug:
			main.show_cover()

	if SettingsManager.start_section:
		# TODO: Jump to start section
		GBE.process_section(SettingsManager.start_section)
	else:
		# Start adventure
		GBE.continue()


func _process(delta):
	# Scrolling takes place after processing
	if _item_added:
		_refresh_story_scroll(auto_scroll)
		_item_added = false
	if _cleared and _next_section != null:
		# Continue execution
		var dest = _next_section[0]
		var module = _next_section[1]
		if not module:
			module = null
		# Stop sound effects
		sfx_player.stop()
		GBE.process_section(dest, module)
		_cleared = false
		_next_section = null


func _refresh_story_scroll(automatic_scroll):
	var vscrollbar = get_children()[2]
	set_v_scroll(vscrollbar.get_max())
	if not automatic_scroll:
		set_v_scroll(0)


func _clear_all_items():
	for child in $StoryContainer.get_children():
		child.queue_free()
	_cleared = true
	# Workaround to avoid oversizing of scrollcontainer when scroll is removed and added again
	rect_size = initial_size


func _select_option(dest, module):
	_debug("Dest: %s Module: %s" % [dest, module])
	# Clear current options
	current_options.clear()
	# Clear vbox
	_next_section = [dest, module]
	# Animation will call _clear_all_items on finishing
	tweener.fade_out(self, section_change_time, funcref(self, "_clear_all_items"))

func _debug(message):
	if SettingsManager.debug:
		add_debug_text_item(message)
		print_debug(message)

#-------------------------------------------------------------------
# GBE Signal handlers
#-------------------------------------------------------------------

func _on_GBE_processed_unknown_macro(macro, section, metadata):
	if macro.begins_with("%miniapp "):
		# Handle miniapp
		add_miniapp_item(macro, section, metadata)
	else:
		_debug("Processed unknown macro %s at section %s. Metadata: %s" % [macro, section, metadata])


func _on_GBE_caught_error(code, message):
	_debug("ERROR (Err: %s): %s" % [code, message])


func _on_GBE_got_plugin_signal(signal_name, data):
	_debug("Plugin signal %s: %s" % [signal_name, data])


func _on_GBE_processed_section(section, metadata, interrupted):
	if interrupted:
		_debug("Finished processing section %s. Interrupted." % section)
		_debug("Section metadata: %s" % metadata)
	else:
		_debug("Finished processing section %s" % section)
		_debug("Section metadata: %s" % metadata)
		# Add options spacer
		add_spacer_item()
		# Add options
		if current_options:
			for option in current_options:
				add_option_item(option)
		else:
			# This is an implicit end
			_debug("There are no options. Reached story ending")
			add_story_end_item()
		# Handle metadata
		handle_metadata(section, metadata)
		# Activate input
		set_process_input(true)
		# Show reading area
		tweener.fade_in(self, section_change_time)
		# Save current state
		GBE.save_state(SettingsManager.GAME_STATE_PATH)
		# Hide or show back button if needed
		if GBE.get_history().size() > 1:
			main.get_node("BackButton").show()
		else:
			main.get_node("BackButton").hide()
	_debug("Current state %s" % GBE.get_serialized_state())
	

func _on_GBE_processed_text(text):
	add_text_item(text)


func _on_GBE_processed_jump(destination):
	_debug("Jumping to %s" % destination)


func _on_GBE_processed_conditional(cond, eval, next):
	_debug("Conditional (%s = %s) to %s" % [cond, eval, next])


func _on_GBE_processed_option(text, dest, module):
	current_options.append({
		"text": text,
		"dest": dest,
		"module": module})
	if not module:
		module = "Current"
	_debug("Option: %s -> %s(%s)" % [text, dest, module])


func _on_GBE_processed_character_speech(charname, text):
	add_character_item(charname, text)


func _on_GBE_processed_styled_text(style, text):
	add_text_item("[%s] %s" % [style, text])


func _on_GBE_processed_media(media, parms):
	if media.ends_with(".jpg") or media.ends_with(".png"):
		add_image_item(media, parms)
	elif media.ends_with(".ogg"):
		play_music(media, parms)
	elif media.ends_with(".wav"):
		play_sfx(media, parms)
	elif media.ends_with(".tscn"):
		add_scene_item(media, parms)
	else:
		_debug("WARNING - Unknown media type: %s" % media)


func _on_GBE_processed_ending():
	# Story end item is added implicitly on no options
	_debug("Reached story ending")


#-------------------------------------------------------------------
# Elements and representations
#-------------------------------------------------------------------

func add_miniapp_item(macro, current_section, metadata):
	_debug("Adding miniapp %s" % macro)
	# Extract miniapp type, solving section, cancel section (maybe it is not present)
	# Show button to open miniapp
	

func add_spacer_item(space=75):
	var item = Control.new()
	$StoryContainer.add_child(item)
	item.size_flags_horizontal = Control.SIZE_EXPAND_FILL
	item.rect_min_size = Vector2(0, space)
	item.mouse_filter = Control.MOUSE_FILTER_PASS
	_item_added = true


func add_image_item(media, parms):
	var item = image_item_scene.instance()
	$StoryContainer.add_child(item)
	item.init(self, media, parms)
	add_spacer_item(DEFAULT_ITEM_SPACING)

func add_debug_text_item(text):
	var item = text_item_scene.instance()
	$StoryContainer.add_child(item)
	item.init(self, '[color=red]DEBUG: ' + text + '[/color]')
	add_spacer_item(DEFAULT_ITEM_SPACING)


func add_text_item(text):
	var item = text_item_scene.instance()
	$StoryContainer.add_child(item)
	item.init(self, text)
	add_spacer_item(DEFAULT_ITEM_SPACING)


func add_option_item(option):
	var item = option_item_scene.instance()
	$StoryContainer.add_child(item)
	item.init(self, option)
	add_spacer_item(DEFAULT_ITEM_SPACING)


func add_character_item(identifier, text):
	add_spacer_item(DEFAULT_ITEM_SPACING +20)
	var item = character_item_scene.instance()
	$StoryContainer.add_child(item)
	item.init(self, identifier, text)
	add_spacer_item(DEFAULT_ITEM_SPACING)


func add_scene_item(scene_name, parms):
	var item = scene_item_scene.instance()
	$StoryContainer.add_child(item)
	item.init(self, scene_name, parms)
	add_spacer_item(DEFAULT_ITEM_SPACING)


func add_story_end_item():
	var item = story_end_item_scene.instance()
	$StoryContainer.add_child(item)
	item.init(self)


#-------------------------------------------------------------------
# UI Events
#-------------------------------------------------------------------

func _set_background_cb(parms):
	var texture = parms[0]
	main.get_node("Background").set_texture(texture)
	tweener.fade_from_black(background, background_change_time, null, null, true)

#-------------------------------------------------------------------
# Methods and actions
#-------------------------------------------------------------------

func handle_metadata(section, metadata):
	# Background change
	var bg = null
	if "bg" in metadata:
		bg = metadata["bg"]
	change_background(bg)
	# Music playing
	var music = null
	if "music" in metadata:
		music = metadata["music"]
		play_music(music, null)
	# Update map position
	if "map" in metadata:
		if "location" in metadata:
			$MapViewer.update_map(metadata["map"], metadata["location"])


func play_sfx(sfx, parms=null):
	var audio = load(SettingsManager.GBE_MEDIA_PATH + "/" + sfx)
	sfx_player.set_stream(audio)
	sfx_player.play()
	_debug("Playing SFX %s - %s" % [audio, SettingsManager.GBE_MEDIA_PATH + "/" + sfx])


func play_music(music, parms=null):
	"""
	Used when in metadata it is specified a music element
	"""
	var audio = load(SettingsManager.GBE_MEDIA_PATH + "/" + music)
	music_player.set_stream(audio)
	music_player.play()
	_debug("Playing music %s - %s" % [audio, SettingsManager.GBE_MEDIA_PATH + "/" + music])


func change_background(bg=null):
	"""
	Used when in metadata it is specified a bg element
	"""
	var texture = null
	if bg:
		texture = load(SettingsManager.GBE_MEDIA_PATH + "/" + bg)
	tweener.fade_to_black(background, background_change_time, funcref(self, "_set_background_cb"), [texture], true)


func stop_sounds():
	sfx_player.stop()
	music_player.stop()


func reset_story():
	main.show_cover(false)
	tweener.fade_to_black(get_parent(), 1, funcref(self, "reset_story_cb"), null, false)


func reset_story_cb():
	main.hide_cover()
	rewind_story()
	tweener.fade_from_black(get_parent(), 1, null, null, false)


func rewind_story(index=0):
	change_background()
	if main.menu_status:
		main._on_MenuButton_pressed()
	current_options.clear()
	for child in $StoryContainer.get_children():
		child.queue_free()
	GBE.rewind(index)
	GBE.continue()


func step_back():
	var history = GBE.get_history()
	var index = history.size() - 1
	if index > 0:
		rewind_story(index - 1)

func reload_current_section():
	var history = GBE.get_history()
	var index = history.size() - 1
	rewind_story(index)


func change_font(font=null, reload=true):
	if not font:
		font_index += 1
		if font_index + 1 > fonts.size():
			font_index = 0
		font = fonts[font_index]
	SettingsManager.set_setting("display", "font_size", font_index)
	main.theme.set_default_font(font)
	
	if reload:
		self.reload_current_section()
