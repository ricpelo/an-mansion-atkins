#
# Add a signal when quitting from game
# Specially useful in mobile games
#

extends Node

signal quitting_game
signal device_back_button_pressed

func _ready():
	# Disable auto quit
	get_tree().set_auto_accept_quit(false)
	# Disable back button quit
	get_tree().set_quit_on_go_back(false)

func _notification(notification):
	# Check if we are closing the game
	if notification == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		emit_signal('quitting_game')
		
	if notification == MainLoop.NOTIFICATION_WM_GO_BACK_REQUEST:
		emit_signal('device_back_button_pressed')
