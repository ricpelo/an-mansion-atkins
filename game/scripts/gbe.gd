extends Node

# Variables
var _plugins_path = "res://gbe_plugins"
var _story_path = "res://story"

# Source code for evaluations
var _eval_source_template = """
# Context
%s

# Plugin helpers
%s

# General helpers
func choice(values):
	randomize()
	return values[(randi() %% 3)]

# Section related helpers
func total_visited():
	return _visited_sections.size()

func visited(section):
	return section in _visited_sections

func first_time_here():
	return _visited_sections.back() == _current_section \\
		and _visited_sections.count(_current_section) == 1

# Internal expression evaluation method
func _eval():
	# Expression to be evaluated
	return %s
"""

# Regular expressions for language parsing
var _lang_regexps = {
	"section": Utils.setup_regex("^(?<section>@\\w+)(?<start>\\*)*(?<metadata> \\{.*\\})*$"),
	"jump": Utils.setup_regex("^>(?<dest>@\\w+)$"),
	"option": Utils.setup_regex("^\\[\\[(?<text>.+)\\|(?<module>.+:)*(?<dest>@\\w+)\\]\\]$"),
	"conditional": Utils.setup_regex("^\\?\\((?<cond>.+)\\) (?<next>.+)$"),
	"set_var": Utils.setup_regex("^=(?<var>\\w+) (?<expr>.*)$"),
	"substitution_expr": Utils.setup_regex("(\\\\)*\\$\\{(?<expr>[^\\$]+)\\}"),
	"char_speech": Utils.setup_regex("^-(?<charname>\\w+) (?<text>.*)$"),
	"styled_text": Utils.setup_regex("^\\.(?<style>\\w+) (?<text>.+)$"),
	"media": Utils.setup_regex("^!(?<media>[\\w\\-\\.\\/]+)(?<parms> .+)*$")
}

var _macro_regexps = {
	"metadata": Utils.setup_regex("^%metadata (?<metadata>.*)$"),
	"load_plugin": Utils.setup_regex("^%load_plugin (?<plugin>\\w+)$"),
	"set_var": Utils.setup_regex("^%set_var (?<var>\\w+) (?<expr>.*)$"),
	"jump": Utils.setup_regex("^%jump (?<dest>@\\w+)$"),
	"char_speech": Utils.setup_regex("^%char_speech (?<charname>\\w+) (?<text>.*)$"),
	"media": Utils.setup_regex("^%media (?<media>[\\w\\-\\.\\/]+)(?<parms> .+)*$")
}

# Errors
const ERR_INVALID_SYNTAX = 1
const ERR_SECTION_NOT_FOUND = 2
const ERR_FAIL_OPENING_TWINE_FILE = 3
const ERR_PLUGIN_ALREADY_LOADED = 4
const ERR_MACRO_NOT_FOUND = 5
var _error_messages = {
	ERR_INVALID_SYNTAX: "Invalid syntax: %s",
	ERR_SECTION_NOT_FOUND: "Section %s was not found in story",
	ERR_FAIL_OPENING_TWINE_FILE: "Can't open or parse Twine file.",
	ERR_PLUGIN_ALREADY_LOADED: "Plugin %s is already registered",
	ERR_MACRO_NOT_FOUND: "Macro not found: %s",
}

# Internal variables
var _story = {}
var _history = []
var _current_module = null
var _start_section = null
var _current_section = null
var _current_line = null
var _visited_sections = []
var _state = {}
var _plugins = {}
var _metadata = []

# Engine signals
signal processed_section(section, metadata, interrupted)
signal processed_text(text)
signal processed_option(text, dest, module)
signal processed_jump(dest)
signal processed_ending()
signal processed_conditional(cond, eval, next)
signal processed_char_speech(charname, text)
signal processed_styled_text(style, text)
signal processed_media(media, parms)
signal processed_unknown_macro(macro)
signal got_plugin_signal(signalname, parms)
signal caught_error(code, desc)
signal loaded_file(path)
# TODO: signal loaded_url(url)


func _parse_twine_file(parser):
	"""
	Parses a twine story using a XMLParser
	Returns: A story buffer readable by load_story
	"""
	var story_buffer = ""
	var startnode = ""
	while parser.read() == 0:
		match parser.get_node_type():
			XMLParser.NODE_ELEMENT:
				if parser.get_node_name() == "tw-storydata":
					startnode = parser.get_named_attribute_value_safe("startnode")
				if parser.get_node_name() == "tw-passagedata":
					# Get passage name
					var section = parser.get_named_attribute_value("name")
					var pid = parser.get_named_attribute_value("pid")
					if pid == startnode:
						section += "*"
					# Read next node (passage contents)
					parser.read()
					# Get node data
					var text = parser.get_node_data()
					# Update story buffer
					story_buffer += "%s\n%s\n" % [section, text]
	return story_buffer


func _get_plugins_eval_helpers():
	var helpers = ""
	for plugin in _plugins.values():
		helpers += plugin.get_eval_helpers()
		helpers += "\n\n"
	return helpers


func _get_eval_context(ctx):
	"""
	Generates the context source code for eval_expression
	The context used is the _state and given context variable
	Returns: Source code string with context variables definitions
	"""
	# Serialization
	var context = "var _visited_sections = %s\n" % Utils.serialize_to_gdscript(_visited_sections)
	context += "var _current_section = %s\n" % Utils.serialize_to_gdscript(_current_section)
	context += "var _current_section_metadata = %s\n" % Utils.serialize_to_gdscript(_story[_current_section]["metadata"])
	for vars in [_state, ctx]:
		for varname in vars.keys():
			var value = Utils.serialize_to_gdscript(_state[varname])
			context += "var %s = %s\n" % [varname, value]
	# Get plugins eval context
	var plugin_context = {}
	for plugin in _plugins.keys():
		plugin_context[plugin] = _plugins[plugin].get_eval_context()
	context += "var plugins = %s" % Utils.serialize_to_gdscript(plugin_context)
	return context


func _expression_substitution(text, ctx={}):
	"""
	Substitutes GDScript expressions in a text by their results
	Returns: The text with all substitutions done
	"""
	var results = _lang_regexps["substitution_expr"].search_all(text)
	for result in results:
		var subs = result.get_string()
		if subs.begins_with("$"):
			var expr = result.get_string(2)
			var value = eval_expression(expr, ctx)
			text = text.replace(subs, value)
	return text


func _process_line(line, section, metadata={}):
	"""
	Process a line statement in a section
	Returns: true if the section processing needs to be interrupted
	"""
	_current_line = line
	if line.begins_with("#"):
		# This is a comment. We just ignore it
		pass
	elif line.begins_with(">"):
		# Jump to another section
		var result = _lang_regexps["jump"].search(line)
		if result:
			var dest = result.get_string(1)
			emit_signal("processed_jump", dest)
			process_section(dest)
			# No more lines will be parsed in this section
			return true
	elif line.begins_with("="):
		# Variable assignment
		var result = _lang_regexps["set_var"].search(line)
		if result:
			var variable = result.get_string(1)
			var expr = result.get_string(2)
			var value = eval_expression(expr)
			_state[variable] = value
	elif line.begins_with("-"):
		# Character speech
		var result = _lang_regexps["char_speech"].search(line)
		if result:
			var charname = result.get_string(1)
			var text = result.get_string(2)
			text = _expression_substitution(text)
			emit_signal("processed_char_speech", charname, text)
	elif line.begins_with("."):
		# Character speech
		var result = _lang_regexps["styled_text"].search(line)
		if result:
			var style = result.get_string(1)
			var text = result.get_string(2)
			text = _expression_substitution(text)
			emit_signal("processed_styled_text", style, text)
	elif line.begins_with("[[") and line.ends_with("]]"):
		# Handle as option
		var result = _lang_regexps["option"].search(line)
		if result:
			var text = result.get_string(1)
			var module = result.get_string(2)
			var dest = result.get_string(3) 
			if module:
				# If the module exists we need to remove the colon at the end
				module = module.substr(0, module.length() - 1)
			emit_signal("processed_option", text, dest, module)
	elif line.begins_with("?"):
		# Conditional statement
		var result = _lang_regexps["conditional"].search(line)
		if result:
			var cond = result.get_string(1)
			var eval = eval_expression(cond)
			var next = result.get_string(2)
			emit_signal("processed_conditional", cond, eval, next)
			if eval:
				return _process_line(next, section, metadata)
	elif line.begins_with("!"):
		# Media
		var result = _lang_regexps["media"].search(line)
		if result:
			var media = result.get_string(1)
			var parms = result.get_string(2)
			if parms:
				parms = parms.strip_edges().split(" ")
			else:
				parms = []
			emit_signal("processed_media", media, parms)
	elif line.begins_with("%"):
		# Story end
		return _process_macro(line, section, metadata)
	else:
		# Handle as text with expression substitution
		var text = _expression_substitution(line)
		emit_signal("processed_text", text)


func _process_macro(macro, section, metadata={}):
	"""
	Process a macro
	"""
	if macro.begins_with("%metadata"):
		var result = _macro_regexps["metadata"].search(macro)
		if result:
			var meta = result.get_string(1)
			var new_metadata = JSON.parse(meta).result
			_metadata.pop_front()
			_metadata.push_front(new_metadata)
		else:
			emit_error_signal(ERR_INVALID_SYNTAX, macro)
	elif macro.begins_with("%load_plugin"):
		var result = _macro_regexps["load_plugin"].search(macro)
		if result:
			var plugin = result.get_string(1)
			load_plugin(plugin)
		else:
			emit_error_signal(ERR_INVALID_SYNTAX, macro)
	elif macro.begins_with("%set_var"):
		var result = _macro_regexps["set_var"].search(macro)
		if result:
			var variable = result.get_string(1)
			var expr = result.get_string(2)
			var value = eval_expression(expr)
			_state[variable] = value
		else:
			emit_error_signal(ERR_INVALID_SYNTAX, macro)
	elif macro.begins_with("%jump"):
		var result = _macro_regexps["jump"].search(macro)
		if result:
			var dest = result.get_string(1)
			emit_signal("processed_jump", dest)
			process_section(dest)
			# No more lines will be parsed in this section
			return true
		else:
			emit_error_signal(ERR_INVALID_SYNTAX, macro)
	elif macro.begins_with("%char_speech"):
		var result = _macro_regexps["char_speech"].search(macro)
		if result:
			var charname = result.get_string(1)
			var text = result.get_string(2)
			text = _expression_substitution(text)
			emit_signal("processed_char_speech", charname, text)
		else:
			emit_error_signal(ERR_INVALID_SYNTAX, macro)
	elif macro.begins_with("%media"):
		var result = _macro_regexps["media"].search(macro)
		if result:
			var media = result.get_string(1)
			var parms = result.get_string(2)
			if parms:
				parms = parms.strip_edges().split(" ")
			else:
				parms = []
			emit_signal("processed_media", media, parms)
		else:
			emit_error_signal(ERR_INVALID_SYNTAX, macro)
	elif macro.begins_with("%nohistory"):
		var index = _history.size() - 1
		if index > 0:
			_history = Utils.array_slice(_history, 0, index - 1)
	elif macro == "%END":
		# Story ending
		emit_signal("processed_ending")
	else:
		# Pass the macro to loaded plugins
		for plugin in _plugins.values():
			if plugin.process_macro(macro, section, metadata):
				return
		emit_signal("processed_unknown_macro", macro, section, metadata)


#------------------------------------------------------
# Public methods
#------------------------------------------------------

func set_plugins_path(path):
	"""
	Sets default plugin path
	"""
	_plugins_path = path


func set_story_path(path):
	"""
	Sets default plugin path
	"""
	_story_path = path


func load_plugin(name, path=null):
	"""
	Load a plugin
	"""
	if path == null:
		path = _plugins_path
	if not name in _plugins:
		var plugin_path = path + "/" + name + "/plugin.gd"
		var script = load(plugin_path)
		_plugins[name] = script.new()
		_plugins[name].init(self)
	else:
		emit_error_signal(ERR_PLUGIN_ALREADY_LOADED, name)


func get_plugin(plugin_name):
	if plugin_name in _plugins:
		return _plugins[plugin_name]
	return null


func load_story_buffer(buffer):
	"""
	Load a story from a .gbe story format buffer
	Returns: A story dictionary with all the sections as keys and their contents as values
	"""
	var story = {}
	var starting = null
	var lines = buffer.split("\n")
	var section = null
	for line in lines:
		line = line.strip_edges()
		if line.begins_with("@"):
			# Parse section statement
			var result = _lang_regexps["section"].search(line)
			if result:
				var section_name = result.get_string(1)
				var section_start = result.get_string(2)
				if section_start:
					starting = section_name
				var meta = result.get_string(3)
				var metadata = null
				if meta:
					metadata = JSON.parse(meta).result
				if not metadata:
					metadata = {}
				story[section_name] = {
					"lines": [],
					"metadata": metadata
				}
				section = section_name
		else:
			if section != null and line != "":
				story[section]["lines"].append(line)
	return [story, starting]


func load_file(path):
	"""
	Loads a story file autodetecting the format
	"""
	if path.get_extension() == "html":
		load_twine_file(path)
	else:
		load_story_file(path)


func load_twine_file(path):
	"""
	Load the story from a Twine HTML story file
	"""
	var parser = XMLParser.new()
	if parser.open(path) != 0:
		emit_error_signal(ERR_FAIL_OPENING_TWINE_FILE)
		pass
	var buffer = _parse_twine_file(parser)
	var data = load_story_buffer(buffer)
	_current_module = path
	_story = data[0]
	_start_section = data[1]
	emit_signal("loaded_file", _current_module)


func load_story_file(path):
	"""
	Load a story from .gbe story file
	"""
	var buffer = Utils.get_file_contents(path)
	var data = load_story_buffer(buffer)
	_current_module = path
	_story = data[0]
	_start_section = data[1]
	emit_signal("loaded_file", _current_module)


func eval_expression(expr, ctx={}):
	"""
	Evaluates a GDScript expression with given context
	Context is generated using _get_eval_context
	Returns: Result of the evaluation
	"""
	# Initialize a GDScript object
	var src = _eval_source_template % [
		_get_eval_context(ctx),
		_get_plugins_eval_helpers(),
		expr]
	var obj = Utils.get_script_object(src)
	# Execute our code and return the result
	var data = obj._eval()
	return data


func get_serialized_state():
	"""
	Generate a serialized state JSON string
	"""
	var plugins_state = {}
	for plugin in _plugins.keys():
		plugins_state[plugin] = _plugins[plugin].get_state()
	return JSON.print({
		"timestamp": Utils.get_timestamp(),
		"section": _current_section,
		"module": _current_module,
		"state": _state,
		"visited": _visited_sections,
		"plugins": plugins_state
	})


func load_serialized_state(state_data):
	
	"""
	Loads the state from a previously serialized state
	"""
	var result = JSON.parse(state_data)
	if result.error == OK:
		var data = result.result
		# Load module
		load_file(data["module"])
		# Load state data
		_state = data["state"]
		# Load visited sections
		_visited_sections = data["visited"]
		# Set current section
		_current_section = data["section"]
		# Restore plugins state
		for plugin in data["plugins"].keys():
			load_plugin(plugin)
			_plugins[plugin].set_state(data["plugins"][plugin])
		return data
	return null


func reset_state():
	"""
	Reset story state
	"""
	_history = []
	_state = {}
	_visited_sections = []
	_current_section = null
	_current_line = null
	_plugins = {}


func check_state(path):
	return Utils.load_json_file(path) != null


func save_state(path):
	var serialized_data = JSON.print(_history)
	Utils.set_file_contents(path, serialized_data)


func load_state(path):
	_history = Utils.load_json_file(path)
	if _history:
		return load_serialized_state(_history.back()[1])
	return null


func start():
	"""
	Start story clearing current state and processing the start section
	"""
	reset_state()
	process_section(_start_section)


func continue():
	"""
	Continue story processing current section without adding it to history
	"""
	if _current_section:
		process_section(_current_section, null, false)
	else:
		start()


func rewind(index):
	_history = Utils.array_slice(_history, 0, index)
	load_serialized_state(_history[index][1])


func process_section(section, module=null, add_to_history=true):
	"""
	Process a section and execute each statement in it
	"""
	if module != null:
		var path = "%s/%s" % [_story_path, module]
		load_file(path)
	# Check section exists
	if not section in _story.keys():
		emit_error_signal(ERR_SECTION_NOT_FOUND, section)
		return
	_current_section = section
	var current_metadata = _story[section]["metadata"]
	if add_to_history:
		# Save history before processing section lines
		_visited_sections.append(section)
		var title = null
		if "title" in current_metadata:
			title = current_metadata["title"]
		_history.append([title, get_serialized_state()])
	_metadata.push_front(current_metadata)
	for line in _story[section]["lines"]:
		# Process each line
		if _process_line(line, section, current_metadata):
			# Returning "true" indicates this section processing
			# needs to be interrupted
			emit_signal("processed_section", section, _metadata.pop_front(), true)
			return
	# Section has been processed
	emit_signal("processed_section", section, _metadata.pop_front(), false)


func process_line(line):
	"""
	Process a single story line
	"""
	_process_line(line, null)


func emit_error_signal(code, data=null, msg=null):
	"""
	Emits an error signal with given code
	"""
	if data:
		if msg:
			msg = msg % data
		elif code in _error_messages and "%" in _error_messages[code]:
			msg = _error_messages[code] % data
	emit_signal("caught_error", code, msg)


func emit_plugin_signal(signalname, parms):
	"""
	Emits a plugin signal
	"""
	emit_signal("got_plugin_signal", signalname, parms)


func get_start_section():
	"""
	Method to get name of start section
	"""
	return _start_section


func get_history():
	"""
	Get current section history
	"""
	return _history


func get_current_state():
	"""
	Get current state
	"""
	return _state


func get_current_module():
	"""
	Get current module name
	"""
	return _current_module


func get_current_section():
	"""
	Get current section name
	"""
	return _current_section


func get_current_line():
	"""
	Get current line
	"""
	return _current_line